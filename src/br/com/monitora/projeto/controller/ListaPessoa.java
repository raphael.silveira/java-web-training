package br.com.monitora.projeto.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.monitora.projeto.model.Pessoa;
import br.com.monitora.projeto.model.PessoaPersistence;

@WebServlet(urlPatterns="/busca")
public class ListaPessoa extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		/*String filtro = null;//req.getParameter("filtro");
		
		Collection<Pessoa> pessoa = new PessoaPersistence().buscaPorSimilaridade(filtro);
		
		req.setAttribute("pessoa", pessoa);*/
		
		PrintWriter writer = resp.getWriter();
		
		writer.println("<html><body>");
			
		String filtro = null;
				
		Collection<Pessoa> pessoa = new PessoaPersistence().buscaPorSimilaridade(filtro);
		writer.println("<ul> <table border=" + 1 + ">");
		for (Pessoa pessoa2 : pessoa) {
			writer.println("<tr>");
			writer.println("<td>" + pessoa2.getNome() + "</td>");
			writer.println("<td>" + pessoa2.getEmail() + "</td>");
			writer.println("<td>" + pessoa2.getIdade() + "</td>");
			writer.println("<td>" + pessoa2.getSexo() + "</td>");
			writer.println("<td>" + pessoa2.getPeso() + "</td>");
			writer.println("</tr>");
		}
		writer.println("</ul> <br/> ");
		
		writer.println("<a href=\"index.jsp\">Voltar ao inicio</a> <br/>");
		
		writer.println("</html></body>");
		//RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/paginas/listarpessoas.jsp");
		
		//dispatcher.forward(req, resp);
		
	}
	
}
