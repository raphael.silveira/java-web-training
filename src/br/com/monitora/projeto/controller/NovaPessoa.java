package br.com.monitora.projeto.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.monitora.projeto.model.Pessoa;
import br.com.monitora.projeto.model.PessoaPersistence;

@WebServlet(urlPatterns= "/novapessoa")
public class NovaPessoa extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String nome = req.getParameter("nome");
		String email = req.getParameter("email");
		String idade = req.getParameter("idade");
		String sexo = req.getParameter("sexo");
		String peso = req.getParameter("peso");
		
		Pessoa pessoa = new Pessoa(nome, email, idade, sexo, peso);
		
		new PessoaPersistence().adiciona(pessoa);	
		
		req.setAttribute("pessoa", pessoa);
		
		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/paginas/novapessoa.jsp");
		
		dispatcher.forward(req, resp);

	}

}
