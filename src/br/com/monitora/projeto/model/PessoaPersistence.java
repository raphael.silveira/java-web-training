package br.com.monitora.projeto.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PessoaPersistence {
	
	public final static HashMap<String, Pessoa> PESSOAS = new HashMap<>();
	
	private static int idPessoa = 0;
	
	public void adiciona(Pessoa pessoa) {
		adicionaPessoa (pessoa);
	}

	private static void adicionaPessoa(Pessoa pessoa) {
		idPessoa = PESSOAS.size() + 1;
		pessoa.setId(idPessoa);
		PESSOAS.put("" + idPessoa + "", pessoa);
	}
	
	public Collection<Pessoa> buscaPorSimilaridade(String nome) {
		if (nome == null)
			return PESSOAS.values();
		
		List<Pessoa> similares = new ArrayList<>();
		for (Pessoa pessoa : PESSOAS.values()) {
			if (pessoa.getNome().toLowerCase().contains(nome.toLowerCase()))
				similares.add(pessoa);
		}
		return similares;
	}

}
